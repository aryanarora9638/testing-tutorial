let { sum, addOnlyEven } = require('./index')

//Test runner
test('sum of two positive nums', () => {
    let solution = sum(2,2)
    //Assertion Library
    expect(solution).toBe(4)
})

test('sum of two negative nums', () => {
    let solution = sum(-1,-3)
    expect(solution).toBe(-4)
})

test('sum of two nulls', () => {
    let solution = sum(null,null)
    expect(solution).toBe(0)
})


//Since all the unit/components have been tested individually, now we can test those functions
//that uses these components to verify that the components can actually work together
//this technique of unit testing before integration helps in locating the bug in the application

test('add only even', () => {
    let nums = [1,2,3,4,5,6,7,8,9,10]
    //here the the function addOnlyEven internally calls the sum function
    //since the sum function has be unit tested already, we can be sure that if the error occurs during this test
    //it will be something else and not sum function
    let solution = addOnlyEven(nums)
    expect(solution).toBe(30)
})