/*
* Testing is an important component of any project,
* Can be an overkill in case of a small application but it very helpful in a long run
*
* Test can be of types ->
* Unit testing - Full isolated (eg - testing a single function)
* Integration testing - With Dependencies (eg - testing a function that calls a function)
* End-to-End (E2E) testing - Full flow (eg - validating the dom after a click)
*
* Complexity=      easy -> medium -> hard
*                  Unit -> Integration -> E2E (increasing complexity, decreasing frequency)
* Frequency= write alot -> couple -> few of these
* complexity refers to how difficult is it to write the test
* frequency refers to how often we write these in an application
*
*
* How to test?
* 3 requirements(tools) for testing
* Test Runner - Execute tests, summarize results eg - Mocha
* Assertion Library - Defining Test logic, conditions eg - Chai
* Headless browser - simulates browser integration eg - Puppeteer
*
* For unit and integration testing we need test runner and assertion library
* For E2E testing we need headless browser
* Newer library that can do both Test runner and Assertion Library  eg - jest
* */

//Unit tests - test each single component individually
let sum = (num1,num2) => {
    return num1 + num2
}

module.exports.sum = sum

//Integration test - test if the the components works together in a bigger function
exports.addOnlyEven = (nums) => {
    let sum = 0;
    nums.forEach(num => {
        if( (num%2) == 0) sum = this.sum(sum,num)
    })
    return sum
}

//E2E test with puppeteer -
//https://www.youtube.com/watch?v=r9HdJ8P6GQI - 29:20
